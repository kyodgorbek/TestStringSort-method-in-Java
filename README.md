# TestStringSort-method-in-Java




import javax.swing.JOptionPane;

public class TestStringSort {

 public static void main(String[] args){
  
  String[] names = new String[5];
  
  // Read in 5 names
  for (int i = 0; i < names.length; i++)
  names[i] = JOptionPane.showInputDialog("Enter name" + (i + 1));
  
  // Sort the array names
  
  // Display  the names in alphabetical order
  System.out.println("Names in alphabetical order:");
  for (int i = 0; i < names.length; i++)
    System.out.println(names[i]);
   }
  }  
